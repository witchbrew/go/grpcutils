module gitlab.com/witchbrew/go/grpcutils

go 1.15

require (
	github.com/google/uuid v1.1.2 // indirect
	github.com/rs/zerolog v1.20.0 // indirect
	gitlab.com/witchbrew/go/errorhandling v0.0.0-20201011115201-3517a6876788 // indirect
	google.golang.org/grpc v1.32.0 // indirect
)
