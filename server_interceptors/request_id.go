package server_interceptors

import (
	"context"
	"github.com/google/uuid"
	"google.golang.org/grpc"
)

type requestIDCtxKey struct{}

func RequestID(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	requestID := uuid.New().String()
	ctxWithRequestID := context.WithValue(ctx, requestIDCtxKey{}, requestID)
	return handler(ctxWithRequestID, req)
}

func GetRequestID(ctx context.Context) string {
	if ctx == nil {
		return ""
	}
	if reqID, ok := ctx.Value(requestIDCtxKey{}).(string); ok {
		return reqID
	}
	return ""
}