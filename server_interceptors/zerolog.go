package server_interceptors

import (
	"context"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
)

func Zerolog(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	logger := log.With().Str("gRPCMethod", info.FullMethod).Logger()
	ctxWithLogger := logger.WithContext(ctx)
	return handler(ctxWithLogger, req)
}

func ZerologRequestID(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	logger := log.Ctx(ctx)
	requestID := GetRequestID(ctx)
	if requestID != "" {
		l := logger.With().Str("requestID", requestID).Logger()
		logger = &l
	}
	ctxWithLogger := logger.WithContext(ctx)
	return handler(ctxWithLogger, req)
}

func ZerologLogServerInfo(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	logger := log.Ctx(ctx)
	logger.Debug().Str("Method", info.FullMethod).Msg("gRPC request")
	resp, err := handler(ctx, req)
	if err != nil {
		s, ok := status.FromError(err)
		if ok {
			logger.Debug().Uint32("code", uint32(s.Code())).Str("errMsg", s.Message()).Msg("gRPC error")
		} else {
			logger.Debug().Str("errMsg", err.Error()).Msg("unknown gRPC error")
		}
	} else {
		logger.Debug().Msg("success")
	}
	return resp, err
}
