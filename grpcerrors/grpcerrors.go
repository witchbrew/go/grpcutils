package grpcerrors

import (
	"gitlab.com/witchbrew/go/errorhandling"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func ToHandledError(e error) *errorhandling.HandledError {
	st, ok := status.FromError(e)
	if !ok {
		return errorhandling.Error(errorhandling.Unknown, e.Error())
	}
	var f func(string) *errorhandling.HandledError
	switch st.Code() {
	case codes.InvalidArgument:
		f = errorhandling.BadInputError
	case codes.NotFound:
		f = errorhandling.NotFoundError
	default:
		f = func(message string) *errorhandling.HandledError {
			return errorhandling.Error(errorhandling.Unknown, message)
		}
	}
	return f(e.Error())
}

func Process(e error) error {
	handledErr, ok := e.(*errorhandling.HandledError)
	if ok {
		return ProcessHandled(handledErr)
	} else {
		return status.Error(codes.Unknown, e.Error())
	}
}

func ProcessHandled(e *errorhandling.HandledError) error {
	var f func(string) error
	switch e.Type {
	case errorhandling.BadInput:
		f = InvalidArgument
	case errorhandling.NotFound:
		f = NotFound
	case errorhandling.Unknown:
		f = Intenal
	default:
		f = Unknown
	}
	return f(e.Message)
}

func Error(code codes.Code, message string) error {
	return status.Error(code, message)
}

func Errorf(code codes.Code, format string, args ...interface{}) error {
	return status.Errorf(code, format, args...)
}

func InvalidArgument(message string) error {
	return Error(codes.InvalidArgument, message)
}

func InvalidArgumentf(format string, args ...interface{}) error {
	return Errorf(codes.InvalidArgument, format, args...)
}

func NotFound(message string) error {
	return Error(codes.NotFound, message)
}

func NotFoundf(format string, args ...interface{}) error {
	return Errorf(codes.NotFound, format, args...)
}

func Intenal(message string) error {
	return Error(codes.Internal, message)
}

func Internalf(format string, args ...interface{}) error {
	return Errorf(codes.Internal, format, args...)
}

func Unknown(message string) error {
	return Error(codes.Unknown, message)
}

func Unknownf(format string, args ...interface{}) error {
	return Errorf(codes.Unknown, format, args...)
}
